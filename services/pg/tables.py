import os
from datetime import datetime


import sqlalchemy as sa


metadata = sa.MetaData()

user_table = sa.Table(
    'users',
    metadata,
    sa.Column('id', sa.Integer, sa.Sequence('user_id_seq'), primary_key=True),
    sa.Column('username', sa.String, nullable=False),
)
subject_table = sa.Table(
    'subjects',
    metadata,
    sa.Column('id', sa.Integer, sa.Sequence('subject_id_seq'), primary_key=True),
    sa.Column('name', sa.String, nullable=False),
)
subscribe_table = sa.Table(
    'subscribes',
    metadata,
    sa.Column('id', sa.Integer, sa.Sequence('subscribe_id_seq'), primary_key=True),
    sa.Column('user_id', sa.Integer, nullable=False),
    sa.Column('subject_id', sa.Integer, nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.ForeignKeyConstraint(['subject_id'], ['subjects.id'], ),
)

message_table = sa.Table(
    'messages',
    metadata,
    sa.Column('id', sa.Integer, sa.Sequence('message_id_seq'), primary_key=True),
    sa.Column('text', sa.String, nullable=False),
    sa.Column('datetime', sa.DateTime, nullable=False, default=datetime.now),
    sa.Column('subject_id', sa.Integer, nullable=False),
    sa.ForeignKeyConstraint(['subject_id'], ['subjects.id'], ),
)

if __name__ == '__main__':
    engine = sa.create_engine(os.getenv('DATABASE_DNS'), echo=True)
    metadata.drop_all(bind=engine)
    metadata.create_all(bind=engine)
