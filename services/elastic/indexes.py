import os

import requests


class Indexes:
    def __init__(self, indexes=None, domain=None):
        self.indexes = indexes or []
        self.domain = domain or os.getenv('ELASTIC_SERVERS')

    def create_all(self):
        for index in self.indexes:
            url = '%s/%s/?pretty' % (self.domain, index)
            requests.put(url)


if __name__ == '__main__':
    pass
    #Indexes(['messages', 'subjects', 'users', ]).create_all()
