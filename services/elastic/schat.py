import asyncio
import aiohttp

from services.interfaces.ichat import (IBaseChatService,
                                       IBaseMessageChatService,)


class FetchMixin:
    async def fetch(self, session, url, method='get', **kw):
        async with session.request(method, url, **kw) as response:
            return await response.json()


class ElChatService(IBaseChatService, FetchMixin):
    def __init__(self, domain):
        self.index = 'subjects'
        self.domain = domain

    def create_query_match(self, query_dict, match_type='match_all'):
        return {
            'query': {
                match_type: query_dict
            }
        }

    async def get_subject_by_name(self, name):
        async with aiohttp.ClientSession() as session:
            url = '%s/%s/_search' % (self.domain, self.index)
            query_match = self.create_query_match({'name': name}, 'match')

            json = await self.fetch(
                session, url, 'get',
                headers={'Content-Type': 'application/json'},
                json=query_match,
            )
            print(json)

            return json

    async def create_subject(self, name):
        async with aiohttp.ClientSession() as session:
            url = '%s/%s/_doc' % (self.domain, self.index)
            subject = {
                'name': name
            }
            json = await self.fetch(
                session, url, 'post',
                json=subject,
            )
            print(json)

            return json

    async def get_or_create_subject(self, name):
        raise NotImplementedError

    async def get_subjects(self):
        async with aiohttp.ClientSession() as session:
            url = '%s/%s/_search' % (self.domain, self.index)
            query_match = self.create_query_match({}, 'match_all')

            json = await self.fetch(
                session, url, 'get',
                headers={'Content-Type': 'application/json'},
                json=query_match,
            )
            print(json)

            return json


class ElMessageChatService(IBaseMessageChatService, FetchMixin):
    async def save_subject_message(self, text, subject_id):
        async with aiohttp.ClientSession() as session:
            url = '%s/%s/_doc' % (self.domain, self.index)
            subject = {
                'text': text,
                'subject_id': subject_id,
            }
            json = await self.fetch(
                session, url, 'post',
                json=subject,
            )
            print(json)

            return json

    async def get_subject_messages(self, subject_id, count=10):
        raise NotImplementedError
