import os
from aiohttp import web
import importlib as imlib

import aiohttp_jinja2
import jinja2

from config import settings


class WebApplication:
    def __init__(self):
        self.app = web.Application()
        self.routes = web.RouteTableDef()
        aiohttp_jinja2.setup(self.app,
                             loader=jinja2.FileSystemLoader(settings.TEMPLATES_FOLDER_PATH))
        self.apps = settings.INSTALLED_APPS

    def register_routes(self):
        for app_name in self.apps:
            app = imlib.import_module(app_name)
            app_views = imlib.import_module(app.__name__ + '.views')
            app_urls = imlib.import_module(app.__name__ + '.urls')

            for url, func_name in app_urls.urlpatterns:
                view_func = getattr(app_views, func_name)
                try:
                    for method in view_func.methods:
                        self.app.add_routes([
                            getattr(web, method)(url, view_func)
                        ])
                except AttributeError:
                    self.app.add_routes([web.get(url, view_func)])

    def up(self):
        web.run_app(self.app)


def main():
    try:
        import hupper

        hupper.start_reloader('server.main')
    except ImportError:
        pass

    app = WebApplication()
    app.register_routes()
    app.up()


if __name__ == "__main__":
    main()
