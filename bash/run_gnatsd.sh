#!/bin/sh

export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

echo "$GOPATH"
echo "$PATH"

exec gnatsd
