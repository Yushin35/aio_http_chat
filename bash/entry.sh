#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z db 5432; do
  sleep 0.1
done

python /app/services/pg/tables.py || exit 1
python /app/services/elastic/indexes.py || exit 2

exec "$@"
