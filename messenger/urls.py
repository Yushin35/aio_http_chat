urlpatterns = [
    ('/', 'chat'),
    ('/room/{subject_name}', 'room'),
    ('/ws/room/{subject_name}', 'ws_room'),
    ('/history/room/{subject_name}', 'hourly_history'),
    ('/history/room/{subject_name}/{hour}', 'message_history'),
]
