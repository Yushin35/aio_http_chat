import aiohttp
from aiohttp import web

from nats.aio.client import Client as NATS

import aiohttp_jinja2
import jinja2

from core.decorators import allow_methods
from core.shortcuts import render

from config.settings import DSN
from config.settings import NATS_SERVERS
from config.settings import ELASTIC_SERVERS

from services.pg.schat import ChatService, MessageChatService
from services.elastic.schat import ElChatService
from services.console import Info


class Client:
    def __init__(self, websocket, message_chat_service, subject):
        self.websocket = websocket
        self.message_chat_service = message_chat_service
        self.subject = subject

    async def cd(self, msg):
        await self.message_chat_service.save_subject_message(msg.data.decode('utf-8'), self.subject[0])
        await self.websocket.send_str(msg.data.decode('utf-8'))

    async def send_unread_message(self, count=10):

        messages = await self.message_chat_service.get_subject_messages(self.subject[0], count)
        for message in messages:
            await self.websocket.send_str(message[1])


@allow_methods(['get', 'post'])
async def chat(request):
    chat_service = ChatService(DSN)
    el_chat_service = ElChatService(ELASTIC_SERVERS)

    subjects = await chat_service.get_subjects()
    await el_chat_service.get_subjects()

    return render(request, 'index.html', context={
        'subjects': subjects,
    })


async def hourly_history(request):
    subject_name = request.match_info['subject_name']
    hours = [hour for hour in range(1, 25)]
    return render(request, 'hourly_history.html', context={
        'subject_name': subject_name,
        'hours': hours,
    })


async def message_history(request):
    subject_name = request.match_info['subject_name']
    return render(request, 'message_history.html', context={
        'subject_name': subject_name,
    })


@allow_methods(['get', ])
async def room(request):
    subject_name = request.match_info['subject_name']

    return render(request, 'chat_room.html', context={
        'subject_name': subject_name,
    })


@allow_methods(['get', ])
async def ws_room(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    nc = NATS()
    chat_service = ChatService(DSN)
    message_chat_service = MessageChatService(DSN)
    await nc.connect(
        servers=[NATS_SERVERS],
        connect_timeout=2,
    )
    if nc.is_connected:
        Info.nats_server_connect(NATS_SERVERS)

    subject_name = request.match_info['subject_name']
    subject = await chat_service.get_or_create_subject(subject_name)

    client = Client(ws, message_chat_service, subject)
    sid = await nc.subscribe(subject_name, cb=client.cd)

    await client.send_unread_message(5)

    while True:
        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                    await nc.publish(subject_name, bytes(msg.data.encode('utf-8')))
                    await nc.flush()
            elif msg.type == aiohttp.WSMsgType.ERROR:
                print('ws connection closed with exception %s' %
                      ws.exception())
