from functools import wraps


def allow_methods(methods=['get', ]):
    def decorated_view(view):
        view.__dict__['methods'] = methods

        @wraps(view)
        def wrapped(*args, **kwargs):
            return view(*args, **kwargs)

        return wrapped

    return decorated_view
