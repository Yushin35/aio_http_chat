import aiohttp_jinja2


def render(request, template, context={}):
    return aiohttp_jinja2.render_template(
        template,
        request,
        context,)
