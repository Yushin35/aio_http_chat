FROM python:3.7-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add --virtual python3-dev \
    && apk add build-base postgresql-client postgresql-dev jpeg-dev libpng-dev \
    && apk add libffi-dev \
    && pip install psycopg2

COPY . /app
WORKDIR /app

RUN pip install --trusted-host pypi.python.org -r req.txt

EXPOSE 8080

ENTRYPOINT ["/app/bash/entry.sh"]

CMD ["python", "server.py"]